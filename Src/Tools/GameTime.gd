extends Node

class Date:
	func _init(d : int, h : int, m : int):
		self.day = d
		self.hour = h
		self.minute = m
		
	func _to_string() -> String:
		var h : String = str(self.hour)
		var d : String = str(self.day)
		var m : String = str(self.minute)
		
		if h.length() < 2:
			h = "0" + h
		if m.length() < 2:
			m = "0" + m
			
		return tr("DAY") + " " + d + ", " + h + ":" + m
	
	var day : int
	var hour : int
	var minute : int

var now : Date

signal day_elapsed

func _init():
	reset()
	
func elapsed_min():
	return self.now.minute + self.now.hour * 60 + self.now.day * 60 * 24

func reset():
	self.now = Date.new(1, 8, 0)
	
func next():
	var elapsed_min = elapsed_min()
	self.now.minute += 1
	
	if self.now.minute >= 60:
		self.now.minute = 0
		self.now.hour += 1
	if self.now.hour >= 24:
		self.now.hour = 0
		self.now.day += 1

func next_day():
	for i in range(0, 60*24):
		next()
	emit_signal("day_elapsed", self.now)
