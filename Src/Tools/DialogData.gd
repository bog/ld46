extends Node

var _initial : Array = ["HELLO", "GOODBYE"]
var _next : Array = ["HOW_TODAY", "AUSCULTATE", "ASK_PRESCRIPTION", "GOODBYE"]

var ask : Dictionary = {
	"initial": _initial,
	"AUSCULTATE": _next,
	"ASK_PRESCRIPTION": _next,
	"HELLO": _next,
	"GOODBYE": ["SEE_YOU_1D",
				"SEE_YOU_3D", 
				"SEE_YOU_1W"],
	"SEE_YOU_1D": _initial,
	"SEE_YOU_3D": _initial,
	"SEE_YOU_1W": _initial,
	"HOW_TODAY": _next,
}

var response : Dictionary = {
	"HELLO": {
		"terrible": "HELLO_TERRIBLE",
		"bad": "HELLO_BAD",
		"ok": "HELLO_OK",
		"good": "HELLO_GOOD",
		"great": "HELLO_GREAT"	
	},
	
	"GOODBYE": {
		"terrible": "GOODBYE_TERRIBLE",
		"bad": "GOODBYE_BAD",
		"ok": "GOODBYE_OK",
		"good": "GOODBYE_GOOD",
		"great": "GOODBYE_GREAT"	
	},
	
	"HOW_TODAY": {
		"terrible": "FEEL_TERRIBLE",
		"bad": "FEEL_BAD",
		"ok": "FEEL_OK",
		"good": "FEEL_GOOD",
		"great": "FEEL_GREAT"
	},
	
	"AUSCULTATE": {
		"terrible": "",
		"bad": "",
		"ok": "",
		"good": "",
		"great": ""
	}
	
}
