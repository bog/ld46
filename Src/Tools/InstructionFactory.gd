extends Node

func _ready():
	pass

var disease_duration : float = 60.0 * 24.0 * 31.0 * 12.0 # one year

func disease() -> Instruction:
	var d = Instruction.new()
	d.label = "plague"
	d.dosage_count = -1
	d.is_disease = true
	randomize()
	var data = $"/root/PatientData"
	var trembling_effect = Function.new()
	trembling_effect.add_sub_function(0.0, 
						disease_duration,
						 Function.linear_lerp(0.0,
											  rand_range(0, 1)))
	d.add_effect(Effect.new(data.Trembling,
							0.0, 
							1.0, 
							trembling_effect))
							
	var heartbeatrate_effect = Function.new()
	heartbeatrate_effect.add_sub_function(0.0, 
						disease_duration,
						 Function.linear_lerp(0.0,
											  rand_range(0, 100)))
	d.add_effect(Effect.new(data.HeartbeatRate,
							0.0, 
							1.0, 
							heartbeatrate_effect))
	
	var temperature_effect = Function.new()
	temperature_effect.add_sub_function(0.0, 
						disease_duration,
						 Function.linear_lerp(0.0,
											  rand_range(0, 100)))
	d.add_effect(Effect.new(data.Temperature,
							0.0, 
							1.0, 
							temperature_effect))
							
	return d

func prescription(item_name : String, count : int, per_day : int) -> Instruction:
	var d = Instruction.new()
	d.label = item_name
	d.dosage_count = count
	d.dosage_per_day = per_day
	d.is_disease = false
	
	var data = $"/root/PatientData"
	var how_fast : float = 300.0
	var decrease = Function.new()
	var decrease_fast = Function.new()
	
	decrease.add_sub_function(0.0, 
						disease_duration,
						 Function.linear_lerp(0.0,
											  1.0))
	decrease_fast.add_sub_function(0.0, 
						disease_duration,
						 Function.linear_lerp(0.0,
											  how_fast))
											
	var increase_fast = Function.new()
	var increase = Function.new()

	increase.add_sub_function(0.0, 
						disease_duration,
						 Function.linear_lerp(0.0,
										  -1.0))
	increase_fast.add_sub_function(0.0, 
						disease_duration,
						 Function.linear_lerp(0.0,
										  -how_fast))									
	var effects = {
		tr("BLEEDING"): [
			Effect.new(data.Temperature, 0.0, 100.0, decrease_fast),
			Effect.new(data.Trembling, 0.0, 1.0, increase),
			Effect.new(data.HeartbeatRate, 0.0, 100.0, decrease_fast)
		],
		tr("CAUTERIZATION"): [
			Effect.new(data.Temperature, 0.0, 100.0, increase_fast),
			Effect.new(data.Trembling, 0.0, 1.0, decrease),
			Effect.new(data.HeartbeatRate, 0.0, 100.0, increase_fast)
		],
		tr("DREAMWEED"): [
			Effect.new(data.Trembling, 0.0, 1.0, increase),
			Effect.new(data.HeartbeatRate, 0.0, 100.0, increase_fast)
		],
		tr("PURGE"): [
			Effect.new(data.Trembling, 0.0, 1.0, decrease),
			Effect.new(data.HeartbeatRate, 0.0, 100.0, decrease_fast)
		],
		tr("WARM_BATH"): [
			Effect.new(data.Temperature, 0.0, 100.0, increase_fast),
			Effect.new(data.Trembling, 0.0, 1.0, increase),
			Effect.new(data.HeartbeatRate, 0.0, 100.0, decrease_fast)
		],
		tr("COLD_BATH"): [
			Effect.new(data.Temperature, 0.0, 100.0, decrease_fast),
			Effect.new(data.Trembling, 0.0, 1.0, decrease),
			Effect.new(data.HeartbeatRate, 0.0, 100.0, increase_fast)
		]
	}
	
	for key in effects.keys():
		if key == item_name:
			for effect in effects[key]:
				d.add_effect(effect)
			
	return d
