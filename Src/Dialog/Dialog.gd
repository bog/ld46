extends Control

var current : String = "initial"
onready var factory = $"/root/DialogData"

var disease : Instruction
var instructions : Array
var state : State
var last_day : bool = false
var game_over : bool = false

# finish at the end of the 50 the day
const VICTORY_DAY_COUNT : float = 50.0
const VICTORY_TIME : float = 60.0 * 24.0 * VICTORY_DAY_COUNT


func _ready():
	# TODO MOVE IN GAME SCENE
	GameTime.reset()
	$Panel/VBoxContainer/TitleHBoxContainer/NameLabel.text = ""
	self.state = State.new()
	update_day()
	self.disease = $"/root/InstructionFactory".disease()
	add_child(self.disease)
	$Panel/ScrollContainer/ItemList.connect("item_activated", self, "_on_response")
	self.disease.start()
	update_view()
	$"/root/GameTime".connect("day_elapsed", self, "_on_day_elapsed")
	
	$Note/TextEdit.text += "# " + str($"/root/GameTime".now) + "\n\n"
	$Note/TextEdit.text += tr("NOTE_BEGINNING") + "\n\n"
	
	$Guide/RichTextLabel.bbcode_enabled = true
	$Guide/RichTextLabel.bbcode_text = "[b]" + tr("GUIDE") + "[/b]"
	$Guide.rect_size.x = OS.window_size.x * 0.4
	
	$Panel/BgTextureRect.rect_size = rect_size

func _process(delta):
	_on_day_elapsed($"/root/GameTime".now)

func update_view():
	var items = $Panel/ScrollContainer/ItemList
	items.clear()
	
	if not self.state.is_alive():
		$Panel/VBoxContainer/TextHBoxContainer/Label.text = "[" + tr("DEAD") + "]"
	
	items.rect_min_size = Vector2(get_viewport_rect().size.x, 
								  get_viewport_rect().size.y)
	
	var font = DynamicFont.new()
	font.font_data = load("res://Assets/FreeMono.otf")
	font.size = 64.0
	items.set("custom_font", font)
	var data = self.factory.ask.get(self.current, null)
	if data != null:
		for question in data:
			items.add_item(tr(question))

func update_game_time():
	var days = {
		"SEE_YOU_1D": 1,
		"SEE_YOU_3D": 3,
		"SEE_YOU_1W": 7
	}
	if self.current == "ASK_PRESCRIPTION":
		var panel = preload("res://PrescribeMenu/PrescribeMenu.tscn").instance()
		add_child(panel)
		panel.connect("add_prescription", self, "_on_add_prescription")
		panel.show()
		
	if self.current == "AUSCULTATE":
		var dialog : AcceptDialog = AcceptDialog.new()
		
		dialog.set("window_title", tr("PATIENT_STATE"))
		var patient_state : String = ""
		patient_state += tr("TEMPERATURE") + " : " + self.state.approx_temperature() + "\n"
		patient_state += tr("HEARTBEATRATE") + " : " + self.state.approx_heartbeat_rate() + "\n"
		patient_state += tr("TREMBLING") + " : " + self.state.approx_trembling()
		
		dialog.set("dialog_text", patient_state)
		add_child(dialog)
		dialog.popup_centered()
		dialog.show()
		
	if days.has(self.current):
		for i in range(0, days[self.current]):
			$"/root/GameTime".next_day()
			self.disease.apply(self.state)
			for instruction in self.instructions:
				instruction.apply(self.state)
	
		$Note/TextEdit.text += "\n\n# "+ str($"/root/GameTime".now) +"\n"
		$AnimationPlayer.play("Fade")
	
func update_day():
	$Panel/VBoxContainer/TitleHBoxContainer/NameLabel.text = tr("DAY") + " " + str(GameTime.now.day)
	
func update_response():
	var label = $Panel/VBoxContainer/TextHBoxContainer/Label
	var responses = $"/root/DialogData".response
	if responses.has(self.current):
		label.text = responses[self.current][self.state.get_state_resume()]
	else:
		label.text = ""
	
func _on_response(id : int):
	if self.game_over:
		return
	
	var res : Array = self.factory.ask.get(self.current, null)
	if res != null:
		self.current = res[id]
		update_response()
		update_game_time()
		update_view()
		
func _on_day_elapsed(now):
	if self.game_over:
		return
	
	if self.state.is_dead() and not self.last_day:
		self.last_day = true
		var dialog : AcceptDialog = AcceptDialog.new()
		dialog.connect("confirmed", self, "_on_failed_msg")
		add_child(dialog)
		dialog.set("window_title", tr("DEATH"))
		dialog.set("dialog_text", tr("THE_KING") + " " + tr("DIED"))
		dialog.popup_centered()
		
		on_game_over()
		dialog.show()
	if GameTime.elapsed_min() > self.VICTORY_TIME and not last_day:
		last_day = true
		var dialog : AcceptDialog = AcceptDialog.new()
		dialog.set("window_title", tr("VICTORY"))
		dialog.set("dialog_text", tr("WELL_DONE"))
		add_child(dialog)
		dialog.connect("confirmed", self, "_on_victory_msg")
		dialog.popup_centered()
		dialog.show()
func on_game_over():
	$DeadHBoxContainer.visible = true
	$Panel/VBoxContainer/TextHBoxContainer/Label.queue_free()
	$Panel/ScrollContainer.queue_free()
	self.game_over = true

func _on_NoteCheckButton_toggled(button_pressed):
	$ScrollAudioStreamPlayer2D.play()
	$Panel/ActionsHBoxContainer/GuideCheckButton.disabled = button_pressed
	$Note.visible = button_pressed

func _on_GuideCheckButton_toggled(button_pressed):
	$ScrollAudioStreamPlayer2D.play()
	$Panel/ActionsHBoxContainer/NoteCheckButton.disabled = button_pressed
	$Guide.visible = button_pressed
	
func _on_add_prescription(count, per_day, item_name):
	var factory = $"/root/InstructionFactory"
	var instruction : Instruction = factory.prescription(item_name, count, per_day)
	add_child(instruction)
	instruction.start()
	self.instructions.push_back(instruction)
	$Note/TextEdit.text += item_name + " : " + str(per_day) + " " + tr("TIMES") + " " + tr("PER_DAY") + " " + tr("DURING") + " " + str(count) + " " + tr("DAYS") + "\n\n"
	
func _on_victory_msg():
	_on_failed_msg()
	
func _on_failed_msg():
	queue_free()
	get_tree().change_scene("res://Main/Main.tscn")
	


func _on_TextEdit_text_changed():
	$WrittingAudioStreamPlayer2D.play()
