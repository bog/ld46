extends Control

signal add_prescription

var item

func _ready():
	
	$Panel/VBoxContainer/CountSpinBox.suffix = tr("DAYS")
	$Panel/VBoxContainer/PerDaySpinBox.suffix = tr("TIMES") + " " + tr("PER_DAY")
	
	var item_list = $Panel/VBoxContainer/ItemList
	item_list.add_item(tr("BLEEDING"))
	item_list.add_item(tr("CAUTERIZATION"))
	item_list.add_item(tr("DREAMWEED"))
	item_list.add_item(tr("PURGE"))
	item_list.add_item(tr("WARM_BATH"))
	item_list.add_item(tr("COLD_BATH"))
	
	self.item = 0
	item_list.select(self.item)
	
func _on_QuitButton_button_down():
	queue_free()


func _on_AcceptButton_button_down():
	var count_value = $Panel/VBoxContainer/CountSpinBox.value

	
	emit_signal("add_prescription", 
				count_value,
				$Panel/VBoxContainer/PerDaySpinBox.value,
				$Panel/VBoxContainer/ItemList.get_item_text(self.item))
	queue_free()


func _on_CountSpinBox_value_changed(value):
	if value <= 0:
		$Panel/VBoxContainer/CountSpinBox.prefix = tr("ALWAYS")
	else:
		$Panel/VBoxContainer/CountSpinBox.prefix = ""


func _on_CheckBox_toggled(button_pressed):
	$Panel/VBoxContainer/CountSpinBox.visible = not button_pressed


func _on_ItemList_item_selected(index):
	self.item = index
