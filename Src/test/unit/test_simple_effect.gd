extends "res://addons/gut/test.gd"

var epsilon : float = 0.001

func test_apply_trembling_effect_on_state():
	var state : State = State.new()
	var fn : Function = Function.new()
	var duration : float = $"/root/InstructionFactory".disease_duration
	
	fn.add_sub_function(0.0, 60.0 * 24.0 * 2, Function.linear_lerp(1.0, 0.0))
	var effect : Effect = Effect.new($"/root/PatientData".Trembling, -100.0, 100.0, fn)
	add_child(effect)
	effect.start_time = $"/root/GameTime".elapsed_min()
	gut.p("0.5 -> " + str(fn.get_value(60.0 * 24.0)))
	assert_almost_eq(1.0, state.trembling, self.epsilon)
	
	$"/root/GameTime".next_day()
	
	effect.apply_effect_on(state)
	
	assert_almost_eq(0.5, state.trembling, self.epsilon)
