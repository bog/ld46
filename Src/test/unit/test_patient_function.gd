extends "res://addons/gut/test.gd"

var FunctionClass
var function : Function

func before_each():
	self.FunctionClass = preload("res://Instruction/Function.gd")
	self.function = self.FunctionClass.new()

func test_simple():
	var linear = self.FunctionClass.linear(4, 7)
	self.function.add_sub_function(0.0, 16.0, linear)

	assert_false(self.function.has_value(-0.1))
	assert_true(self.function.has_value(0.0))
	assert_true(self.function.has_value(0.1))
	
	assert_true(self.function.has_value(8.0))
	
	assert_true(self.function.has_value(15.9))
	assert_true(self.function.has_value(16))
	assert_false(self.function.has_value(16.1))
	
	assert_eq(39, self.function.get_value(8.0))

func test_lerp():
	var linear = self.FunctionClass.linear_lerp(0.0, 32.0)
	self.function.add_sub_function(0.0, 1.0, linear)
	var epsilon : float = 0.1
	
	assert_false(self.function.has_value(-0.1))
	assert_almost_eq(0.0, self.function.get_value(0.0), epsilon)
	assert_almost_eq(16.0, self.function.get_value(0.5), epsilon)
	assert_almost_eq(32.0, self.function.get_value(1.0), epsilon)
	assert_false(self.function.has_value(1.1))
