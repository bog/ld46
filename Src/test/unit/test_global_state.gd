extends "res://addons/gut/test.gd"

var state : State
var epsilon : float = 0.1

func before_each():
	self.state = State.new()
	
func test_initial_values():
	assert_almost_eq(1.0, self.state.global_state(), self.epsilon)

func test_initial_but_trembling():
	self.state.trembling = 0.0
	assert_almost_eq(2.0/3.0, self.state.global_state(), epsilon)
