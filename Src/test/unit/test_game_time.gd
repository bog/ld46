extends "res://addons/gut/test.gd"

var time : GameTime

func before_each():
	self.time = get_node("/root/GameTime")
	self.time.reset()

func test_initial():
	assert_eq(1, self.time.now.day)
	assert_eq(8, self.time.now.hour)
	assert_eq(0, self.time.now.minute)

func test_change_minute():
	for i in range(0, 59):
		self.time.next()
		
	assert_eq(1, self.time.now.day)
	assert_eq(8, self.time.now.hour)
	assert_eq(59, self.time.now.minute)
	
	self.time.next()
	
	assert_eq(1, self.time.now.day)
	assert_eq(9, self.time.now.hour)
	assert_eq(0, self.time.now.minute)
	
func test_change_day():
	for i in range(0, 60 * (24 - 8) - 1):
		self.time.next()
		
	assert_eq(1, self.time.now.day)
	assert_eq(23, self.time.now.hour)
	assert_eq(59, self.time.now.minute)
	
	self.time.next()
	
	assert_eq(2, self.time.now.day)
	assert_eq(0, self.time.now.hour)
	assert_eq(0, self.time.now.minute)
