class_name State

var heartbeat_rate : float
var temperature : float
var trembling : float

const INITIAL_TEMP : float = 37.0
const INITIAL_BPM : float = 70.0
const DEATH_THRESHOLD : float = 0.05
const GLOBAL_DEATH_THRESHOLD : float = 0.1

func _init():
	randomize()
	var mini : float = 0.6
	self.heartbeat_rate = INITIAL_BPM * rand_range(1.0 - mini, 1.0)
	self.temperature = INITIAL_TEMP * rand_range(1.0 - mini, 1.0)
	self.trembling = 1.0 * rand_range(1.0 - mini, 1.0)
	
func temperature_displayed() -> float:
	return 100.0 * temperature_state()
	
func trembling_displayed() -> float:
	return 100.0 - 100.0 * self.trembling
	
func heartbeat_displayed() -> float:
	return 100.0 * self.heartbeat_rate_state()

func _to_string():
	return str(temperature_displayed()) + "C°, " + str(heartbeat_displayed()) + " bpm, " + str(trembling_displayed())

func approx_temperature():
	var dir : String = tr("HIGH")
	if self.temperature < INITIAL_TEMP:
		dir = tr("LOW")
	return str(self.temperature_displayed()) + "% " + dir
	
func approx_heartbeat_rate():
	var dir : String = tr("HIGH")
	if self.heartbeat_rate < INITIAL_BPM:
		dir = tr("LOW")
	return str(self.heartbeat_displayed()) + "% " + dir

func approx_trembling():
	return str(self.trembling_displayed()) + "%"
	
func approx(value : float):
	if value <= 0.2:
		return tr("LARGE0")
	if value <= 0.5:
		return tr("BIG1")
	if value <= 0.6:
		return tr("BIG0")
	if value <= 0.8:
		return tr("SMALL1")
		
	return tr("SMALL0")

func get_state_resume():
	var st : float = [global_state(),
				 heartbeat_rate_state(),
				 self.trembling,
				 temperature_state()].min()

	if st <= 0.2:
		return "terrible"
	elif st > 0.2 and st <= 0.4:
		return "bad"
	elif st > 0.4 and st <= 0.6:
		return "ok"
	elif st > 0.6 and st <= 0.8:
		return "good"
	elif st > 0.8:
		return "great"
		
func is_alive() -> bool:
	return not is_dead()
	
func is_dead() -> bool:
	return self.trembling <= DEATH_THRESHOLD or self.temperature <= DEATH_THRESHOLD or self.heartbeat_rate <= DEATH_THRESHOLD or global_state() <= GLOBAL_DEATH_THRESHOLD 
	
func temperature_state() -> float:
	return 1 - (abs(self.temperature - INITIAL_TEMP)/INITIAL_TEMP)
	
func heartbeat_rate_state() -> float:
	return 1 - (abs(self.heartbeat_rate - INITIAL_BPM)/INITIAL_BPM)
	
func global_state() -> float:
	var sum : float = 0.0
	var count : int = 3
	
	sum += self.trembling
	sum += heartbeat_rate_state()
	sum += temperature_state()
	
	return sum/float(count)
