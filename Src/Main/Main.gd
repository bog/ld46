extends Control

func _ready():
	connect("resized", self, "_on_resize")

func _on_PlayButton_button_down():
	get_tree().change_scene("res://Dialog/Dialog.tscn")
	queue_free()

func _on_QuitButton_button_down():
	get_tree().quit()

func _on_resize():
	$TextureRect.rect_size = rect_size
