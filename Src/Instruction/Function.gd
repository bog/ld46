class_name Function

class MathFunction:
	func _init():
		pass
		
	func compute(x : float) -> float:
		return x

class LinearFunction:
	extends MathFunction
	var a = 0
	var b = 0
	func compute(x : float) -> float:
		return self.a * x + self.b
	
static func linear(a : float, b : float) -> LinearFunction:
	var lf = LinearFunction.new()
	lf.a = a
	lf.b = b
	return lf
	
static func linear_lerp(a : float, b : float) -> LinearFunction:
	return linear(b - a, a)

class Domain:
	var begin : float = 0.0
	var end : float = 0.0
	var function : MathFunction

var domains : Array = []

func _init():
	pass

func add_sub_function(begin : float, 
	end : float,
	fn : MathFunction):
	var domain : Domain = Domain.new()
	domain.begin = begin
	domain.end = end
	domain.function = fn
	self.domains.push_back(domain)

func has_value(x : float) -> bool:
	for dom in self.domains:
		if x >= dom.begin  and x <= dom.end:
			assert(dom.function != null, "function not valid")
			return true
			
	return false
	
func get_value(x : float) -> float:
	for dom in self.domains:
		if x >= dom.begin  and x <= dom.end:
			assert(dom.function != null, "unvalid function")
			return dom.function.compute(x)
	assert(false, "undefined function for x = " + str(x))
	return 0.0

func get_normalized_value(x : float) -> float:
	for dom in self.domains:
		if x >= dom.begin  and x <= dom.end:
			assert(dom.function != null, "unvalid function")
			return dom.function.compute(x/(dom.end-dom.begin))
	assert(false, "undefined function for x = " + str(x))
	return 0.0
