class_name Instruction
extends Node

var label : String = ""
var effects : Array = []

var dosage_count = -1 # -1 => always
var dosage_per_day = 1

onready var is_disease : bool = false
 
func _ready():
	pass

func _to_string() -> String:
	var count : String = str(self.dosage_count) + " " + tr("TIMES")
	if self.dosage_count == -1:
		count = tr("ALWAYS")
		
	return self.label + str(self.dosage_per_day) + " " + tr("PER_DAY") + " " + count

func add_effect(e : Effect):
	self.effects.push_back(e)
	add_child(e)

func start():
	for effect in self.effects:
		effect.start_time = $"/root/GameTime".elapsed_min()

func apply(state : State):
	if self.dosage_count > 0 or self.dosage_count == -1:
		for i in range(0, self.dosage_per_day):
			for effect in self.effects:
				effect.apply_effect_on(state)
		if not self.dosage_count == -1: 
			self.dosage_count -= 1
