class_name Effect

extends Node

var function : Function
var minimum : float
var maximum : float 
var property : int
var start_time : int
var patient_data

func _init(prop : int, mini : float, maxi : float, fn : Function):
	self.property = prop
	self.function = fn
	self.minimum = mini
	self.maximum = maxi
	
func _ready():
	self.patient_data = get_node("/root/PatientData")
	
func get_value(x : float):
	return clamp(self.function.get_value(x), self.minimum, self.maximum)

func apply_effect_on(state: State):
	var prop = self.property
	
	var time = get_node("/root/GameTime")
	var elapsed = time.elapsed_min() - self.start_time
	
	assert(self.function.has_value(elapsed))
	
	var val = self.function.get_normalized_value(elapsed)

	if prop == self.patient_data.HeartbeatRate:
		state.heartbeat_rate -= val
		state.heartbeat_rate = clamp(state.heartbeat_rate, 0.0, 200.0)
	if prop == self.patient_data.Temperature:
		state.temperature -= val
		state.temperature = clamp(state.temperature, 0.0, 100.0)
	if prop == self.patient_data.Trembling:
		state.trembling -= val
		state.trembling = clamp(state.trembling, 0.0, 1.0)

